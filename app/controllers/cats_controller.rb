class CatsController < ApplicationController

  def index
    @cats = Cat.all

    render :index
  end

  def create
    @cat = Cat.create(params[:cat])
    @cat.save!

    redirect_to cat_url(@cat.id)
  end

  def new
    @cat = Cat.new
  end

  def edit
    @cat = Cat.find(params[:id])
  end

  def update
    @cat = Cat.create(params[:id])
    @cat.update_attributes(params[:cat])

    redirect_to cat_url(@cat.id)
  end

  def show
    @cat = Cat.find(params[:id])

    render :show
  end
end
