class Cat < ActiveRecord::Base
  attr_accessible :age, :birth_date, :color, :name, :sex
  validates :age, presence: true, numericality: true
  validates :color, presence: true, inclusion: { in: %w(black white purple),
    message: "%{value} is not a color" }
  validates :sex, presence: true, inclusion: { in: %w(M F),
    message: "%{value} is not a sex" }
  validates :birth_date, presence: true

  has_many :cat_rental_requests,
  class_name: "CatRentalRequest",
  foreign_key: :cat_id

end
