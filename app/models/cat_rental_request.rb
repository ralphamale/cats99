class CatRentalRequest < ActiveRecord::Base
  attr_accessible :cat_id, :start_date, :end_date, :status
  validates :cat_id, null: false
  validates :start_date, null: false
  validates :end_date, null: false
  validates :status, presence: true, inclusion: { in: %w(black white purple),
      message: "%{value} is not a color" }

      #validation for overlapping requests

      #add association between cat rental request and cat.
  belongs_to :cat,
  foreign_key: :cat_id

end
